/* -*- mode:c++ -*-
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/


template<class T> std::tuple<bool,double,double> MuonMatchingTool :: trigPosForMatch(const T *trig) {
  return std::forward_as_tuple(true, trig->eta(), trig->phi());
}


template<class T, class OFFL>
const T* MuonMatchingTool :: match(const OFFL* offl, std::string trig, float reqdR, bool &pass,
                                   std::tuple<bool,double,double> (*trigPosForMatchFunc)(const T*)) const {

  ATH_MSG_DEBUG("MuonMonitoring::match<T>");

  using CONTAINER = DataVector<T>;
  const T* ptr = nullptr;

  const TrigCompositeUtils::LinkInfo<CONTAINER> featureLinkInfo = searchLinkInfo<T>(offl, trig, reqdR, pass, trigPosForMatchFunc);
  if( featureLinkInfo.isValid() ){
    const ElementLink<CONTAINER> link = featureLinkInfo.link;
    ptr = *link;
  }
  
  return ptr;

}


template<class T, class OFFL>
const TrigCompositeUtils::LinkInfo<DataVector<T> > MuonMatchingTool :: searchLinkInfo(const OFFL* offl, std::string trig, float reqdR, bool &pass,
                                                                                      std::tuple<bool,double,double> (*trigPosForMatchFunc)(const T*)) const {

  ATH_MSG_DEBUG("MuonMonitoring::searchLinkInfo<T>");

  using CONTAINER = DataVector<T>;

  double offlEta = offl->eta();
  double offlPhi = offl->phi();

  std::vector< TrigCompositeUtils::LinkInfo<CONTAINER> > featureCont = m_trigDec->features<CONTAINER>( trig, TrigDefs::includeFailedDecisions );
  TrigCompositeUtils::LinkInfo<CONTAINER> muonLinkInfo;
  for(const TrigCompositeUtils::LinkInfo<CONTAINER>& featureLinkInfo : featureCont){
    if ( !featureLinkInfo.isValid() ) continue;
    const ElementLink<CONTAINER> link = featureLinkInfo.link;

    const auto [status, trigEta, trigPhi] = trigPosForMatchFunc(*link);
    if(!status) continue;
    double deta = offlEta - trigEta;
    double dphi = xAOD::P4Helpers::deltaPhi(offlPhi, trigPhi);
    double dR = sqrt(deta*deta + dphi*dphi);

    ATH_MSG_VERBOSE("Trigger muon candidate eta=" << trigEta << " phi=" << trigPhi  << " pt=" << (*link)->pt() << " dR=" << dR);
    if( dR<reqdR ){
      reqdR = dR;
      muonLinkInfo = featureLinkInfo;
      pass = ( featureLinkInfo.state == TrigCompositeUtils::ActiveState::ACTIVE );
      ATH_MSG_DEBUG("* Trigger muon eta=" << trigEta << " phi=" << trigPhi  << " pt=" << (*link)->pt() << " dR=" << dR <<  " isPassed=" << pass);
    }
  }

  return muonLinkInfo;

}
